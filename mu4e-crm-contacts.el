;;; mu4e-crm-contacts.el --- Contact insertion with completing-read-multiple for mu4e  -*- lexical-binding: t; -*-

;; Copyright (C) 2017-2021  Anders Johansson
;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Keywords: mail, convenience
;; URL: http://www.gitlab.com/andersjohansson/mu4e-crm-contacts
;; Modified: 2022-09-13
;; Package-Requires: ((emacs "25.1") (mu4e "1.2"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Bind ‘mu4e-crm-contacts-insert’ to a key you find suitable.

;;; Code:
(eval-when-compile
  (require 'cl-macs))
(require 'mu4e)
(require 'subr-x)

(defcustom mu4e-crm-contacts-alias-file nil
  "Contact aliases file. Will be added first to completion list.

Good for making groups etc. Alias and real expansion is added on
one line separated with tab. Set to nil to disable"
  :group 'mu4e
  :type '(choice (const :tag "Disable" nil)
                 (file :must-match t)))

(defvar mu4e-crm-contacts--notfetched t "Used for checking fetch status.")

(defun mu4e-crm-contacts-insert ()
  "Insert mu4e contacts with the help of ‘completing-read-multiple’."
  (interactive)
  (unless (hash-table-p mu4e--contacts-set)
    (mu4e-crm-contacts-request-contacts-wait))
  (let ((alias-list (mu4e-crm-contacts--get-alias-list)))
    (cl-loop for addr in (completing-read-multiple
                          "Insert contacts: "
                          (lambda (str pred flag) (if (eq flag 'metadata)
                                                      '(metadata . ((display-sort-function . identity)) )
                                                    (funcall (completion-table-dynamic
                                                              (lambda (_str) (hash-table-keys mu4e--contacts-set)))
                                                             str pred flag)
                                                    ;; (delq nil
                                                    ;;       (delete-dups
                                                    ;;        (append
                                                    ;;         alias-list
                                                    ;;         (hash-table-keys mu4e--contacts-set))))

                                                    )))
             collect
             (alist-get addr alias-list addr nil #'equal)
             into addresses
             finally do (insert (string-join addresses ", ")))))

(defun mu4e-crm-contacts--get-alias-list ()
  "Return the list of email aliases."
  (when mu4e-crm-contacts-alias-file
    (with-temp-buffer
      (insert-file-contents mu4e-crm-contacts-alias-file)
      (cl-loop for line in (split-string (buffer-string) "\n" t)
               collect (let ((ll (split-string line "	" t " ")))
                         (cons (car ll) (cadr ll)))))))

(defun mu4e-crm-contacts--signal-fetch (&rest _ignore)
  (setq mu4e-crm-contacts--notfetched nil))

(defun mu4e-crm-contacts-request-contacts-wait ()
  (unwind-protect
      (let ((mu4e-compose-complete-addresses t))
        (advice-add 'mu4e--update-contacts :after #'mu4e-crm-contacts--signal-fetch)
        (mu4e--request-contacts-maybe)
        (while mu4e-crm-contacts--notfetched (sleep-for 0.1)))
    (advice-remove 'mu4e~update-contacts #'mu4e-crm-contacts--signal-fetch)
    (setq mu4e-crm-contacts--notfetched t)))

;;; Hook
(defun mu4e-crm-contacts-hook-insert (&rest _ignore)
  "Select recipients if we are in an empty To-field."
  (when (and (eq major-mode 'mu4e-compose-mode)
             (message-point-in-header-p)
             (save-excursion (beginning-of-line)
                             (looking-at-p "^To:[[:space:]]+$")))
    (mu4e-crm-contacts-insert)))

(defvar mu4e-crm-contacts-insert-functions
  '(mu4e~compose-mail
    mu4e-compose-new
    mu4e-compose-forward)
  "Invocations to automatically launch `mu4e-crm-contacts-insert' after.")

(cl-loop for x in mu4e-crm-contacts-insert-functions
         do (advice-add x :after #'mu4e-crm-contacts-hook-insert))

(provide 'mu4e-crm-contacts)
;;; mu4e-crm-contacts.el ends here
